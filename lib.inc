section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit:
   mov rax, 60
   syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
	xor rax, rax
	.loop:
	  cmp byte [rdi+rax], 0
	  je .exit
	  inc rax
	  jmp .loop
	.exit:
	  ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
	push rdi
    call string_length
	pop rdi
    mov rdx, rax
    mov rax, 1
    mov rsi, rdi
    mov rdi, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rsi, rsp
    pop rdi
    mov rax, 1
    mov rdx, 1
    mov rdi, 1
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, 10
    jmp print_char
    

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax
    mov rax, rdi
    mov r8, 10
    mov r9, rsp
    push 0
    .loop:
	xor rdx, rdx
	div r8
	add rdx, '0'
	dec rsp
	mov byte[rsp], dl
	cmp rax, 0
	je .end
	jmp .loop
    .end:
	mov rdi, rsp
	push r9
	call print_string
	pop r9
	mov rsp, r9
   	ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi, 0
    jl .negative
    jmp print_uint
    

    .negative:
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	jmp print_uint
	
    
; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rdx, rdx    
    .loop:
	mov dl, [rsi]
	mov al, [rdi]
	cmp al, dl
    	je .byte_equals
        jne .not_equals

    .byte_equals:
	cmp dl, 0
    	je .string_equals
        inc rsi
	inc rdi
    	jmp .loop

    .string_equals:
	mov rax, 1
    	jmp .exit

    .not_equals:
	mov rax, 0
	
	
    .exit:
	ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax
    mov rdi, 0
    mov rdx, 1
    push 0
    mov rsi, rsp
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rax, rax
    xor rcx, rcx
    
    .loop:
	cmp rsi, rcx
	jl .fail

	push rcx
	push rsi
	push rdi
	call read_char
	pop rdi
	pop rsi
	pop rcx
	
	cmp rax, 0
	je .end_word

	cmp rax, 0x20
	je .space

	cmp rax, 0xA
	je .space

	cmp rax, 0x9
	je .space
	
	mov [rdi+rcx], rax
	inc rcx
	jmp .loop
    .space:
	cmp rcx, 0
	je .loop
    .end_word:
	mov [rdi+rcx], rax
		
	mov rdx, rcx
	mov rax, rdi
	jmp .exit

    .fail:
	mov rax, 0

    .exit:

    	ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10
    .loop:
	movzx r9, byte[rdi+rcx]
	cmp r9, 0
	je .exit
	cmp r9b, '0'
	jl .exit
	cmp r9b, '9'
	jg .exit


	mul r8
	sub r9b, '0'
	add rax, r9
	inc rcx
	jmp .loop

    .exit:
	mov rdx, rcx
    	ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    mov rcx, rdi
    cmp byte[rcx], '-'
    je .negative
    jmp .positive
    .negative:
	inc rcx
	mov rdi, rcx
	push rcx
	call parse_uint
	pop rcx
	neg rax
	inc rdx
	jmp .exit
    .positive:
	mov rdi, rcx
	call parse_uint
    .exit:
	ret 

; Принимает указатель на строку(rdi), указатель(rsi) на буфер и длину буфера(rdx)
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    xor rcx, rcx


    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi



    mov r8, rax
    cmp rdx, r8
    jl .fail
    .loop:
	cmp rcx, r8
	jg .finish
	
	mov rax, [rdi+rcx]
	mov [rsi+rcx], rax
	inc rcx
	jmp .loop
    .finish:

	mov rax, r8
	jmp .exit
    .fail:
	xor rax, rax
    .exit:
	ret
